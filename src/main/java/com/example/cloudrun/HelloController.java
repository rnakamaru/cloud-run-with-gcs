package com.example.cloudrun;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

  @RequestMapping(value = "/", method = RequestMethod.GET)
  public ResponseEntity<String> receiveMessage() {
    return new ResponseEntity<>("Hello world!!", HttpStatus.OK);
  }
}
